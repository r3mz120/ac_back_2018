<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text = "";
$birth2 = 0;
$facultate = "";
$sex = "";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check) || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

if(strlen($firstname) < 3 || strlen($lastname) < 3){
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}

if(strlen($firstname) >20 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "First or Last name is longer than expected!";
}

if(strlen($question) < 15){
	$error = 1;
	$error_text = "Answer is too short";
}

if(!ctype_alpha($firstname) || !ctype_alpha($lastname)){
	$error = 1;
	$error_text = "First or Last name contains digits";
}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

 if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	 $error = 1;
	$error_text = "Email is not valid";
}

if( strlen($cnp)!=13 || (($cnp[0] != "1") && ($cnp[0] != "2") && ($cnp[0] != "3") && ($cnp[0] != "4") && ($cnp[0] != "5") && ($cnp[0] != "6"))){
	$error = 1;
	$error_text = "Cnp in not valid";
}

if(!filter_var($facebook, FILTER_VALIDATE_URL) !== false){
	$error = 1;
	$error_text = "Facebook is not valid";
}

$birth2 = strtotime($birth);
if(time()-$birth2<18*31536000 || time()-$birth2>100*31536000){
	$error = 1;
	$error_text = "Age is not good";
}

if($captcha_generated !== $captcha_inserted){
	$error = 1;
	$error_text = "Captcha is wrong";
}

if(!ctype_alpha($facultate)){
	$error = 1;
	$error_text = "Facultate contains digits";
}

if(strlen($facultate) < 3 || strlen($facultate) > 30){
	$error = 1;
	$error_text = "Facultate is not good";

}

if((int)$cnp[0]%2==1){
	$sex = "M";
}else{
	$sex = "F";
}
	

if((substr($cnp,-12,2)!==substr($birth,-8,2)) || (substr($cnp,-10,2)!==substr($birth,-5,2))||(substr($cnp,-8,2)!==substr($birth,-2,2))){
	$error = 1;
	$error_text = "Cnp!=birth";
	}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);
	$sql = "SELECT email FROM register2 WHERE email = ? LIMIT 1";
	$stmt = $con->prepare($sql);
	$stmt->execute([$email]);
	$exists = $stmt->fetch();

	if ($exists) {
		$error = 1;
		$error_text = "User is already registered";
	}

	$query = $con->query('SELECT * FROM register2');
	if($query->rowCount()>50){
	$error = 1;
	$error_text = "Too many people";

	}
	

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
if($error == 0){
$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:facultate,:sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':sex',$sex);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}

}
else{
    echo $error_text;
}
